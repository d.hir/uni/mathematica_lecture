(* ::Package:: *)

(* ::Subsubsection:: *)
(*Basic Overview*)


BeginPackage["QuantumComputing`"];


(* ::Text:: *)
(*A package containing functions and macros for simulating a quantum computer.*)


(* ::Text:: *)
(*Objects starting with 'qc' are considered private and can only be used inside this package; Objects starting with 'QC' are public and can be accessed from outside.*)


(* ::Text:: *)
(*Clearing all variables stored in the kernel corresponding to this package.*)


Clear["QuantumComputing`*"];
Clear["QuantumComputing`Private`*"]


(* ::Subsubsection:: *)
(*Usage descriptions*)


(* ::Text:: *)
(*Flags for setting calculation type to Analytic or Numeric.*)


QCSetNumeric::usage = "QCSetNumeric[] instructs the QuantumComputing package to calculate everything numerically";
QCSetAnalytic::usage = "QCSetAnalytic[] instructs the QuantumComputing package to calculate everything analytically";


(* ::Text:: *)
(*Functions for calculating the action of a gate onto a coefficient vector.*)


QCNot::usage = "QCNot[cv] applies a quantum NOT gate (Pauli-X gate) to the coefficient vector v";
QCPauliX::usage = "QCPauliX[cv] applies the Pauli-X gate (quantum NOT gate) to the coefficient vector v";
QCPauliY::usage = "QCPauliY[cv] applies the Pauli-Y gate to the coefficient vector v";
QCPauliZ::usage = "QCPauliZ[cv] applies the Pauli-Z gate (phase-flip) to the coefficient vector v";
QCHadamard::usage = "QCHadamard[cv] applies the Hadamard gate (creates balanced superposition from pure states) to the coefficient vector v";
QCPhaseShiftS::usage = "QCPhaseShiftS[cv] applies the S gate (phase shift by \!\(\*FractionBox[\(\[Pi]\), \(2\)]\)) to the coefficient vector v";
QCPhaseShiftSt::usage = "QCPhaseShiftSt[cv] applies the \!\(\*SuperscriptBox[\(S\), \(\[Dagger]\)]\) gate (phase shift by -\!\(\*FractionBox[\(\[Pi]\), \(2\)]\)) to the coefficient vector v";
QCPhaseShiftT::usage = "QCPhaseShiftT[cv] applies the T gate (phase shift by \!\(\*FractionBox[\(\[Pi]\), \(4\)]\)) to the coefficient vector v";
QCPhaseShiftTt::usage = "QCPhaseShiftTt[cv] applies the \!\(\*SuperscriptBox[\(T\), \(\[Dagger]\)]\) gate (phase shift by -\!\(\*FractionBox[\(\[Pi]\), \(4\)]\)) to the coefficient vector v";
QCCNot::usage = "QCCNot[cv] applies the CNOT gate (controlled NOT gate) to the coefficient vector v";
QCQFT::usage = "QCQFT[cv] computes the Quantum Fourier Transform (QFT) of the input vector v";


(* ::Text:: *)
(*Single-Operation matrices corresponding to the quantum gates.*)


QCMX::usage = "PauliX gate matrix";
QCMY::usage = "PauliY gate matrix";
QCMZ::usage = "PauliZ gate matrix";
QCMNot::usage = "Quantum NOT gate matrix (equivalent to PauliX matrix QCMX)";
QCMH::usage = "Hadamard gate matrix";
QCMS::usage = "S phaseshift gate matrix (phaseshift by \!\(\*FractionBox[\(\[Pi]\), \(2\)]\))";
QCMT::usage = "T phaseshift gate matrix (phaseshift by \!\(\*FractionBox[\(\[Pi]\), \(4\)]\))";
QCMCNot::usage = "CNOT gate matrix";
QCMQFT::usage = "QCMQFT[N] gets Quantum Fourier Transform matrix \!\(\*SubscriptBox[\(F\), \(N\)]\) for coefficient vectors of length N";
QCMiQFT::usage = "QCMiQFT[N] gets inverse Quantum Fourier Transform matrix \!\(\*SuperscriptBox[SubscriptBox[\(F\), \(N\)], \(-1\)]\)=\!\(\*SuperscriptBox[SubscriptBox[\(F\), \(N\)], \(*\)]\) for coefficient vectors of length N";
QCMP0::usage = "Projection matrix P(0) for projecting a 1 qubit state to state \!\(\*TemplateBox[{\"0\"},\n\"Ket\"]\)";
QCMP1::usage = "Projection matrix P(1) for projecting a 1 qubit state to state \!\(\*TemplateBox[{\"1\"},\n\"Ket\"]\)";


(* ::Text:: *)
(*Function returning matrices padded to a n-qubit system.*)


QCMXN::usage = "QCMXN[qubitnumber,totalqubits] returns the transformation matrix for applying the PauliX gate (NOT gate) to the qubitnumber-th qubit of a system with totalqubit number of qubits";
QCMYN::usage = "QCMYN[qubitnumber,totalqubits] returns the transformation matrix for applying the PauliY gate to the qubitnumber-th qubit of a system with totalqubit number of qubits";
QCMZN::usage = "QCMZN[qubitnumber,totalqubits] returns the transformation matrix for applying the PauliZ gate to the qubitnumber-th qubit of a system with totalqubit number of qubits";
QCMHN::usage = "QCMHN[qubitnumber,totalqubits] returns the transformation matrix for applying the Hadamard gate to the qubitnumber-th qubit of a system with totalqubit number of qubits";
QCMSN::usage = "QCMSN[qubitnumber,totalqubits] returns the transformation matrix for applying the phaseshift S gate (\!\(\*TemplateBox[{RowBox[{\"\[Pi]\", \"/\", \"2\"}]},\n\"Canvas\"]\)) to the qubitnumber-th qubit of a system with totalqubit number of qubits";
QCMTN::usage = "QCMTN[qubitnumber,totalqubits] returns the transformation matrix for applying the phaseshift T gate (\!\(\*TemplateBox[{RowBox[{\"\[Pi]\", \"/\", \"4\"}]},\n\"Canvas\"]\)) to the qubitnumber-th qubit of a system with totalqubit number of qubits";
QCMCNotN::usage = "QCMCNotN[controlqubit,invertqubit,totalqubits] returns the transformation matrix for applying the CNot gate to a totalqubits-system";


(* ::Text:: *)
(*Auxiliary Functions useful for debugging and result representation*)


QCGeneralCoefficients::usage = "QCGeneralCoefficients[numqubits,letter] returns coefficients for a numqubits-system with 'letter' as the indexed symbol (e.g. letter = x and numqubits = 2 will give {\!\(\*SubscriptBox[\(x\), \(0\)]\),\!\(\*SubscriptBox[\(x\), \(1\)]\),\!\(\*SubscriptBox[\(x\), \(10\)]\),\!\(\*SubscriptBox[\(x\), \(11\)]\)})";
QCGeneralACoefficients::usage = "QCGeneralACoefficients[numqubits] returns QCGeneralCoefficients[numqubits,a] (i.e. a coefficient vector with 'a' as the indexed symbol)";
QCKetRepresentation::usage = "QCKetRepresentation[cv] returns the representation in a Bit-Ket focused base representation";


(* ::Text:: *)
(*Special Algorithms*)


QCDeutsch::usage = "QCDeutsch[f] applies the Deutsch algorithm to calculate the balanced-ness or constant-ness of the function f";


(*qcRevertQuantumKron::usage = "asdf";*)


QCGetGroverF::usage = "QCGetGroverF[\[Omega]] returns a classical function representing the search query for the Grover algorithm";
QCGrover::usage = "QCGrover[f,numstates] applies the Grover algorithm to the index function f to find the matching index < numstates";


(* ::Subsubsection:: *)
(*Code*)


Begin["`Private`"];


(* ::Text:: *)
(*Code handling flag for numeric and analytic calculation styles.*)


qcNumeric = False;
qcN[expr_] := If[qcNumeric, N[expr], expr];
QCSetNumeric[] := qcNumeric = True;
QCSetAnalytic[] := qcNumeric = False;


(* ::Text:: *)
(*General function for applying a gate as a matrix to a coefficient vector.*)


qcMatrixGate[matrix_, cv_] := Module[{},
  If[Length[cv] != Length[matrix],
    Print["Size does not match. Operation: ", matrix // MatrixForm, cv // MatrixForm]
    ,
    Return[matrix . cv // qcN];
  ]
]


(* ::Text:: *)
(*Definition of various single-action gate matrices.*)


QCMX = PauliMatrix[1];
QCMY = PauliMatrix[2];
QCMZ = PauliMatrix[3];
QCMNot = QCMX;
QCMH = HadamardMatrix[2] // qcN;
QCMS = {{1,0},{0,I}};
QCMT = {{1,0},{0,(1 + I) / Sqrt[2]}} // qcN;
QCMCNot = {{1,0,0,0},{0,1,0,0},{0,0,0,1},{0,0,1,0}};
QCMQFT[numstates_] := 1/Sqrt[numstates] Table[
	Exp[2 Pi I x y / numstates], 
	{x, 0, numstates - 1}, 
	{y, 0, numstates - 1}
] // qcN;
QCMiQFT[numstates_] := Conjugate[QCMQFT[numstates]];
QCMP0 = {{1,0},{0,0}};
QCMP1 = {{0,0},{0,1}};


(* ::Text:: *)
(*Definition of various functions for applying gates to coefficient vectors.*)
(* - Clifford set*)


qcPauliGate[numstates_, cv_] := qcMatrixGate[PauliMatrix[numstates], cv]
QCPauliX[cv_] := qcPauliGate[1, cv]
QCNot[cv_] := QCPauliX[cv]
QCPauliY[cv_] := qcPauliGate[2, cv]
QCPauliZ[cv_] := qcPauliGate[3, cv]
QCHadamard[cv_] := qcMatrixGate[HadamardMatrix[2], cv]
qcPhaseShiftGate[\[CurlyPhi]_, cv_] := qcMatrixGate[{{1,0},{0,Exp[I \[CurlyPhi]]}}, cv]
QCPhaseShiftS[cv_] := qcPhaseShiftGate[Pi/2, cv]
QCPhaseShiftSt[cv_] := qcPhaseShiftGate[-Pi/2, cv]


(* ::Text:: *)
(* - Additional T and SuperDagger[T] gates*)


QCPhaseShiftT[cv_] := qcPhaseShiftGate[Pi/4, cv]
QCPhaseShiftTt[cv_] := qcPhaseShiftGate[-Pi/4, cv]


(* ::Text:: *)
(* - CNot Gate (Controlled NOT gate)*)


QCCNot[cv_] := qcMatrixGate[{{1,0,0,0},{0,1,0,0},{0,0,0,1},{0,0,1,0}}, cv]


(* ::Text:: *)
(*Function for applying a quantum fourier transform to a coefficient vector.*)


QCQFT[cv_] := 
	Module[{numstates, rootofunity, ftmatrix},
		numstates = Length[cv];
		rootofunity = Exp[2 Pi I / numstates];
		ftmatrix = 1/Sqrt[numstates] Table[
			rootofunity ^ (x * y), 
			{x, 0, numstates - 1}, 
			{y, 0, numstates - 1}
		] // qcN;
		qcMatrixGate[ftmatrix, cv]
	]


(* ::Text:: *)
(*Function padding 1-qubit matrix to 'totalqubits'-matrix. 1-qubit matrix is placed for qubit 'qubitnumber'.*)


qcPadSingleStateMatrix[matrix_, qubitnumber_, totalqubits_] := 
	(* qubitnumber starts at 1 *)
	Module[{result, j},
		If[Length[matrix] != 2,
			Print["Cannot pad matrix ", matrix // MatrixForm, " with identities"];
			Return[];
		];
		If[qubitnumber > totalqubits,
			Print["Index of qubit for matrix operation (", qubitnumber, ") cannot exceed total qubit count (", totalqubits, ")"];
			Return[];
		];
		result = {1};
		For[j = 1, j < qubitnumber, j++,
			result = KroneckerProduct[result, IdentityMatrix[2]];
		];
		result = KroneckerProduct[result, matrix];
		For[j = qubitnumber + 1, j <= totalqubits, j++,
			result = KroneckerProduct[result, IdentityMatrix[2]];
		];
		Return[result // qcN];
	]
QCMXN[qubitnumber_, totalqubits_] := qcPadSingleStateMatrix[QCMX, qubitnumber, totalqubits];
QCMYN[qubitnumber_, totalqubits_] := qcPadSingleStateMatrix[QCMY, qubitnumber, totalqubits];
QCMZN[qubitnumber_, totalqubits_] := qcPadSingleStateMatrix[QCMZ, qubitnumber, totalqubits];
QCMHN[qubitnumber_, totalqubits_] := qcPadSingleStateMatrix[QCMH, qubitnumber, totalqubits];
QCMSN[qubitnumber_, totalqubits_] := qcPadSingleStateMatrix[QCMS, qubitnumber, totalqubits];
QCMTN[qubitnumber_, totalqubits_] := qcPadSingleStateMatrix[QCMT, qubitnumber, totalqubits];
qcPadDualStateMatrix[matrixqubit0_, positionmatrixqubit0_, matrixqubit1_, positionmatrixqubit1_, totalqubits_] :=
	Module[{padded0projector, padded1projector, padded0matrix, padded1matrix},
		padded0matrix = qcPadSingleStateMatrix[matrixqubit0, positionmatrixqubit1, totalqubits];
		padded1matrix = qcPadSingleStateMatrix[matrixqubit1, positionmatrixqubit0, totalqubits];
		padded0projector = qcPadSingleStateMatrix[QCMP0, positionmatrixqubit0, totalqubits];
		padded1projector = qcPadSingleStateMatrix[QCMP1, positionmatrixqubit0, totalqubits];
		Return[padded1projector . padded0matrix + padded0projector . padded1matrix];
	]
QCMCNotN[controlqubit_, invertcubit_, totalqubits_] := qcPadDualStateMatrix[QCMNot, controlqubit, IdentityMatrix[2], invertcubit, totalqubits];


(* ::Text:: *)
(*Functions for general coefficients and ket representation.*)


QCGeneralCoefficients[numqubits_, letter_] := Table[Subscript[letter, IntegerString[i,2,numqubits]],{i, 0, 2 ^ numqubits - 1}];
QCGeneralACoefficients[numqubits_] := 
	Module[{},
		If[ValueQ[Global`a],
			Print["Warning: Global Variable 'a' already taken. Using formal '\[FormalA]' instead."];
			Return[QCGeneralCoefficients[numqubits, \[FormalA]]];
		];
		Return[QCGeneralCoefficients[numqubits, Global`a]];
	]
QCKetRepresentation[cv_] := 
	Module[{numqubits, kets},
		numqubits = Log2[Length[cv]];
		If[!IntegerQ[numqubits],
			Print["Argument hat invalid length! Needs to be a power of 2."];
			Return[];
		];
		kets = Table[Ket[IntegerString[i,2, numqubits]], {i, 0, Length[cv] - 1}];
		Return[cv . kets];
	];


(* ::Subsubsection:: *)
(*Special Algorithms*)


(* ::Text:: *)
(*Deutsch Algorithm*)


(*qcRevertQuantumKron[v_] := 
	Module[{v1, v2, sig11, sig12, sig21, sig22, v1p, v2p, diff},
		v1 = {Sqrt[v[[1]]^2 + v[[2]]^2], Sqrt[v[[3]]^2 + v[[4]]^2]};
		v2 = {Sqrt[v[[1]]^2 + v[[3]]^2], Sqrt[v[[2]]^2 + v[[4]]^2]};
		Do[
			v1p = {{sig11, 0}, {0, sig12}} . v1;
			v2p = {{sig21, 0}, {0, sig22}} . v2;
			diff = Norm[Flatten[KroneckerProduct[v1p, v2p]] - v];
			If[diff < 10^-10,
				Return[{v1p, v2p}, Module]
			],
			{sig11, 1, -1, -2},
			{sig12, 1, -1, -2},
			{sig21, 1, -1, -2},
			{sig22, 1, -1, -2}
		];
		Print["Error while reconstructing inverse KroneckerProduct!"];
	]*)
qcDeutschOracle2[f_,v_] :=
	Module[{M, Uf},
		M = {{f[0], 0},{0, f[1]}};
		Uf = KroneckerProduct[M, QCMX] + KroneckerProduct[IdentityMatrix[2] - M, IdentityMatrix[2]];
		Return[Uf . v];
	]
qcMeasureSingleQubit[v_, qubitnumber_, measurementvalue_] :=
	Module[{M, P, state},
		P = Switch[measurementvalue, 
			0, QCMP0, 
			1, QCMP1, 
			_, Message["Measurementvalue invalid! Must be either 0 or 1."]; {{0,0},{0,0}}
		];
		M = qcPadSingleStateMatrix[P, qubitnumber, Log2[Length[v]]];
		state = M . v;
		Return[ConjugateTranspose[state] . state];
	]
QCDeutsch[f_] :=
	Module[{psi1, psi2, psi3, H12, H1, psi4},
		psi1 = KroneckerProduct[{1,0},{0,1}]//Flatten;
		H12 = KroneckerProduct[QCMH, QCMH];
		psi2 = H12 . psi1;
		psi3 = qcDeutschOracle2[f, psi2];
		H1 = qcPadSingleStateMatrix[QCMH,1,2];
		psi4 = H1 . psi3;
		Switch[Chop[qcMeasureSingleQubit[psi4, 1, 1]],
			0, Return["Constant"],
			1, Return["Balanced"],
			_, Print["Error: Indeterminate(", qcMeasureSingleQubit[psi4, 1, 1], ")"]; Return[""];
		]
	]


(* ::Text:: *)
(*Grover Algorithm*)


qcGetGroverOracle[f_, numstates_] :=
	Module[{U},
		U = DiagonalMatrix[Table[(-1)^f[n], {n, 0, numstates - 1}]];
		Return[U];
	]
QCGetGroverF[omega_] := 
	Module[{f},
		f[n_] := If[n == omega, 1, 0];
		Return[f];
	]
qcGetGroverS[numstates_] := ConstantArray[1 / Sqrt[numstates], numstates] // qcN;
qcGetGroverDiffusionOperator[numstates_] :=
	Module[{U, s},
		s = qcGetGroverS[numstates];
		U = IdentityMatrix[numstates] - 2 Outer[Times, s, s];
		Return[-U];
	]
QCGrover[f_, numstates_] := 
	Module[{Uw, Us, state, r, res},
		Uw = qcGetGroverOracle[f, numstates];
		Us = qcGetGroverDiffusionOperator[numstates];
		state = qcGetGroverS[numstates];
		r = Ceiling[Pi / 4 * Sqrt[numstates]];
		res = Nest[N[Us . Uw . #] &, state, r];
		index = Ordering[res, -1][[1]];
		Return[{index - 1, Abs[res[[index]]]^2}];
	]


End[];
EndPackage[];
